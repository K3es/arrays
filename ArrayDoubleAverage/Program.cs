﻿using System;

namespace ArrayDoubleAverage
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var array = new double[10];
            var totalValue = 0.0;
            
            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine($"Please determine the input value for index {i}:");
                array[i] = Convert.ToDouble(Console.ReadLine());
                totalValue += array[i];
            }

            Array.Sort(array);
            Array.Reverse(array);

            Console.WriteLine($"Total number: {totalValue}");
            Console.WriteLine($"Average number: {Math.Round(totalValue / 10, 2)}");
        }
    }
}