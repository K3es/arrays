﻿using System;

namespace ArrayXHtoL
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var array = new int[10];
            
            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine($"Please determine the input value for index {i}:");
                array[i] = Convert.ToInt32(Console.ReadLine());
            }

            Array.Sort(array);
            Array.Reverse(array);

            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine($"Index {i} value: {array[i]}");
            }
        }
    }
}