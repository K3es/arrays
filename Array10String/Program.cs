﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Array10String
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var array = new string[10];
            
            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine($"Please determine the string for index {i}:");
                array[i] = Console.ReadLine();
            }

            IEnumerable<string> sortedArray =
                from stringValue in array
                orderby stringValue.Length, stringValue
                select stringValue;

            Console.Clear();
            foreach (var stringValue in sortedArray)
            {
                Console.WriteLine(stringValue);
            }
        }
    }
}