﻿using System;

namespace ArrayMonths
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var months = new[]
            {
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            };
            
            foreach (var month in months)
            {
                Console.WriteLine($"Result: {month}");
            }
        }
    }
}