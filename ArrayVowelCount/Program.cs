﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ArrayVowelCount
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var array = new string[5];
            var vowelsArray = new HashSet<char> { 'a', 'e', 'i', 'o', 'u', 'y' };

            for (var i = 0; i < 5; i++)
            {
                Console.WriteLine($"Please determine the string for index {i}:");
                array[i] = Console.ReadLine();
            }

            foreach (var stringValue in array)
            {
                var totalVowels = stringValue.ToLower().Count(c => vowelsArray.Contains(c));
                if (totalVowels % 2 != 0)
                {
                    Console.WriteLine(stringValue);
                }
            }
        }
    }
}