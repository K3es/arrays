﻿using System;

namespace Array10
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var array = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            
            for (var i = 1; i <= array.Length; i++)
            {
                Console.WriteLine($"Result: {i}");
            }
        }
    }
}
