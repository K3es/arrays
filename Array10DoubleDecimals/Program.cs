﻿using System;

namespace Array10DoubleDecimals
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var array = new double[10];
            
            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine($"Please determine the input value for index {i}:");
                array[i] = Convert.ToDouble(Console.ReadLine());
            }

            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine($"Index {i} value: {Math.Round(Convert.ToDecimal(array[i]) + 0.00M, 2)}");
            }
        }
    }
}