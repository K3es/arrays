﻿using System;

namespace ArrayXTotal
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var array = new int[10];
            var totalValue = 0;
            
            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine($"Please determine the input value for index {i}:");
                array[i] = Convert.ToInt32(Console.ReadLine());
            }

            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine($"Index {i} value: {array[i]}");
                totalValue += array[i];
            }

            Console.WriteLine($"Total value: {totalValue}");
        }
    }
}